package count_words;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import count_words.actors.MasterActor;

import java.io.File;

/*
* This app allow to find how many times
* some word is appearing in some text.
*
* Word is storing at constant WORD
* Text is storing at constant FILE
 */
public class Solution {
    private static final File DATA = new File("data.txt");

    public static void main(String[] args) throws Exception {
        ActorSystem system = ActorSystem.create();
        ActorRef master = system.actorOf(Props.create(MasterActor.class), "master");
        Thread thread1 = new Thread(new Demo(master, DATA, "scotch"));
        Thread thread2 = new Thread(new Demo(master, DATA, "whiskey"));
        Thread thread3 = new Thread(new Demo(master, DATA, "bourbon"));
        thread1.start();
        thread2.start();
        thread3.start();
    }
}
