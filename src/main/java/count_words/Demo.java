package count_words;

import akka.actor.ActorRef;
import akka.pattern.Patterns;
import akka.util.Timeout;
import count_words.messages.SubscribeData;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import java.io.File;
import java.util.concurrent.TimeUnit;

public class Demo implements Runnable {

    private final ActorRef master;
    private final File data;
    private final String word;
    private static final int LIFETIME_MINUTES = 5;

    public Demo(ActorRef master, File data, String word) {
        this.master = master;
        this.data = data;
        this.word = word;
    }

    @Override
    public void run() {
        try {
            Timeout timeout = new Timeout(Duration.create(LIFETIME_MINUTES, TimeUnit.MINUTES));
            Future<Object> future = Patterns.ask(master, new SubscribeData(data, word), timeout);
            Integer result = (Integer) Await.result(future, timeout.duration());
            System.out.println(String.format("Word '%s' occurs '%d' times at text '%s'", word, result, data));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
