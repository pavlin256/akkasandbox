package count_words.actors;

import akka.actor.*;
import akka.routing.RoundRobinPool;
import count_words.messages.CountInitializer;
import count_words.messages.RoutingInitializer;
import count_words.messages.ResultOfCount;
import count_words.messages.Timeout;
import scala.concurrent.duration.Duration;
import java.io.*;
import java.util.concurrent.TimeUnit;

public class RouteActor extends UntypedActor {

    private final int NUMBER_OF_WORKERS = 5;
    private Cancellable cancelable;
    private boolean isWorkDistributed;
    private int count = 0;
    private int unfinishedCalculations = 0;
    private ActorRef sender;

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof RoutingInitializer) {
            RoutingInitializer msg = (RoutingInitializer) o;
            ActorRef self = getSelf();
            sender = getSender();
            cancelable = getContext().system().scheduler().scheduleOnce(
                    Duration.create(5, TimeUnit.SECONDS),
                    self, new Timeout(), getContext().dispatcher(), null);

            isWorkDistributed = false;
            coordinateWork(msg);
            isWorkDistributed = true;
        } else if (o instanceof ResultOfCount) {
            ResultOfCount result = (ResultOfCount) o;
            count += result.getResult();
            unfinishedCalculations--;
            if (unfinishedCalculations == 0 && isWorkDistributed) {
                cancelable.cancel();
                sender.tell(new ResultOfCount(count), self());
            }
        } else if (o instanceof Timeout) {
            sender.tell(o, self());
        }  else {
            unhandled(o);
        }
    }

    private void coordinateWork(RoutingInitializer msg) {
        try {
            ActorRef router = this.getContext().actorOf(new RoundRobinPool(NUMBER_OF_WORKERS)
                            .props(Props.create(CountActor.class)), "router");

            BufferedReader reader = new BufferedReader(new FileReader(msg.getData()));
            while (reader.ready()) {
                router.tell(new CountInitializer(reader.readLine(), msg.getWord()), self());
                unfinishedCalculations++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
