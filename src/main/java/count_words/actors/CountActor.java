package count_words.actors;

import akka.actor.UntypedActor;
import count_words.messages.CountInitializer;
import count_words.messages.ResultOfCount;

public class CountActor extends UntypedActor {

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof CountInitializer) {
            CountInitializer msg = (CountInitializer) o;
            sender().tell(new ResultOfCount(count(msg.getStr(), msg.getWord())), self());
        } else {
            unhandled(o);
        }
    }

    private int count(String text, String word) {
        word = word.toLowerCase();
        text = clearText(text);
        int result = 0;
        for (String s: text.split(" ")) {
            if (s.equals(word)) {
                result++;
            }
        }
        return result;
    }

    private String clearText(String text) {
        return text.replaceAll("[.,;-?!\":\']", "").toLowerCase();
    }
}
