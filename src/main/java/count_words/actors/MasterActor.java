package count_words.actors;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.actor.UntypedActor;
import count_words.messages.ResultOfCount;
import count_words.messages.RoutingInitializer;
import count_words.messages.SubscribeData;
import count_words.messages.Timeout;
import scala.concurrent.duration.Duration;

import javax.xml.bind.SchemaOutputResolver;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class MasterActor extends UntypedActor {

    private final Map<ActorRef, ActorRef> clients;
    private AtomicInteger id = new AtomicInteger(0);

    public MasterActor() {
        this.clients = new HashMap<>();
    }

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof SubscribeData) {
            SubscribeData data = (SubscribeData) o;
            ActorRef routeActor = getContext().actorOf(Props.create(RouteActor.class), "routeActor" + id.incrementAndGet());
            clients.put(routeActor, getSender());
            routeActor.tell(new RoutingInitializer(data), self());
          } else if (o instanceof ResultOfCount) {
            ActorRef sender = getSender();
            if (clients.get(sender) != null) {
                sender.tell(PoisonPill.getInstance(), self());
                clients.get(sender).tell(((ResultOfCount) o).getResult(), self());
                clients.remove(sender);
            }
        } else if (o instanceof Timeout) {
            ActorRef sender = getSender();
            System.out.println("Actor " + sender + " killed after timeout");
            sender.tell(PoisonPill.getInstance(), self());
            clients.remove(sender);
        }else {
            unhandled(o);
        }
    }
}
