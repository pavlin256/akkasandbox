package count_words.messages;

import java.io.File;

public class SubscribeData {
    private final File data;
    private final String word;

    public SubscribeData(File data, String word) {
        this.data = data;
        this.word = word;
    }

    public File getData() {
        return data;
    }

    public String getWord() {
        return word;
    }
}
