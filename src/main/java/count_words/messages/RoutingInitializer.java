package count_words.messages;

import java.io.File;

public class RoutingInitializer {
    private final File data;
    private final String word;

    public RoutingInitializer(SubscribeData subscribeData) {
        this.data = subscribeData.getData();
        this.word = subscribeData.getWord();
    }

    public File getData() {
        return data;
    }

    public String getWord() {
        return word;
    }
}
