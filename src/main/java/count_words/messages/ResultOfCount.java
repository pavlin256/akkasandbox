package count_words.messages;

public class ResultOfCount {
    private final int result;

    public ResultOfCount(int result) {
        this.result = result;
    }

    public int getResult() {
        return result;
    }
}
