package count_words.messages;

public class CountInitializer {
    private final String str;
    private final String word;

    public CountInitializer(String str, String word) {
        this.str = str;
        this.word = word;
    }

    public String getStr() {
        return str;
    }

    public String getWord() {
        return word;
    }
}
